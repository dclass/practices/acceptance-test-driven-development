class Person {
  constructor(network, location) {
    this.network = network;
    this.location = location;
    this.messages = [];

    this.network.subscribe(this);
  }

  shout(message) {
    this.network.broadcast(message, this.location);
  }

  hears(message) {
    this.messages.push(message);
  }

  messagesHeard() {
    return this.messages;
  }
}

class Network {
  constructor(range) {
    this.range = range;
    this.listeners = [];
  }

  subscribe(person) {
    this.listeners.push(person);
  }

  broadcast(message, shouter_location) {
    this.listeners.forEach((listener) => {
      const isInRange =
        Math.abs(listener.location - shouter_location) <= this.range;

      if (isInRange) {
        if (message.length <= 180) {
          listener.hears(message);
        }
      }
    });
  }
}

exports.Person = Person;
exports.Network = Network;
