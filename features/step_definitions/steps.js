const { Network, Person } = require("../../src/shouty");
const { Given, When, Then, Before } = require("@cucumber/cucumber");
const { assertThat, is } = require("hamjest");
const assert = require("assert");

const default_range = 100;
const default_location = 0;

Before(() => {
  this.network = new Network(default_range);
  this.people = {};
});

Given("a person named {word}", (name) => {
  this.people[name] = new Person(this.network, default_location);
});

Given("the range is {int} meter(s)", (range) => {
  this.network = new Network(range);
});

Given("people are located at", (dataTable) => {
  dataTable
    .transpose()
    .hashes()
    .map(({ name, location }) => {
      this.people[name] = new Person(this.network, location);
    });
});

When("Sean shouts {string}", (message) => {
  this.people["Sean"].shout(message);
  this.messageFromSean = message;
});

When("Sean shouts", () => {
  this.people["Sean"].shout("Hello");
});

When("Sean shouts following message", (message) => {
  this.people["Sean"].shout(message);
});

Then("Lucy hears Sean's message", () => {
  assertThat(this.people["Lucy"].messagesHeard(), is([this.messageFromSean]));
});

Then("Lucy hears a shout", () => {
  assertThat(this.people["Lucy"].messagesHeard().length, is(1));
});

Then("{word} should not hears a shout", (name) => {
  assertThat(this.people[name].messagesHeard().length, is(0));
});

Then("Lucy hears the following messages:", (expectedMessages) => {
  const actualMessages = this.people["Lucy"]
    .messagesHeard()
    .map((message) => [message]);
  assert.deepEqual(actualMessages, expectedMessages.raw());
});
